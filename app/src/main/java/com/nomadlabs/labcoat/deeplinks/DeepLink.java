package com.nomadlabs.labcoat.deeplinks;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class DeepLink extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = getIntent();
        if (i != null) {
            String action = i.getAction();
            Uri uri = i.getData();
            if (Intent.ACTION_VIEW.equals(action) && uri != null) {
                Intent labcoat = new Intent(Intent.ACTION_VIEW, uri.buildUpon().scheme("labcoat").build());
                try {
                    startActivity(labcoat);
                } catch (ActivityNotFoundException e) {
                    //TODO Prompt user to uninstall
                }
            }
        }

        finish();
    }
}
